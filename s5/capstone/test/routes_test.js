const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite_currency', () => {

	// Currency URI:
	it('test_api_post_currency_is_running', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: 'United States Dollar',
      		ex: {
        		peso: 50.73,
        		won: 1187.24,
        		yen: 108.63,
        		yuan: 7.03
    		}
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_name_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		ex: {
        		peso: 50.73,
        		won: 1187.24,
        		yen: 108.63,
        		yuan: 7.03
    		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_name_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: 123
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_name_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_ex_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: 'United States Dollar'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_ex_not_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		ex: 123
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_ex_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		ex: ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_alias_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: 'United States Dollar'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_alias_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		alias: 123
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_400_if_alias_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		alias: ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_all_fields_complete_but_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: 'Japanese Yen',
      		ex: {
	        	peso: 0.47,
	        	usd: 0.0092,
	        	won: 10.93,
	        	yuan: 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_currency_returns_200_if_all_fields_complete_no_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
      		name: 'Singapore Dollar',
      		ex: {
	        	peso: 41,
	        	usd: 0.74,
	        	won: 977.07,
	        	yuan: 5.38
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

})

describe('forex_api_test_suite_rates', () => {

	// Rates URI:
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

})
