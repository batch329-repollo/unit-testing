const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.status(200).send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.status(200).send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(req.body.name === null || req.body.name === undefined || req.body.name === ''){
        	return res.status(400).send({
                'error': 'Bad Request - NAME must not be empty'
            })
        }

        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }
        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            })
        }
        if(req.body.ex === null || req.body.ex === undefined || req.body.ex === ''){
        	return res.status(400).send({
                'error': 'Bad Request - EX must not be empty'
            })
        }

        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }
        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be an string'
            })
        }
        if(req.body.alias === null || req.body.alias === undefined || req.body.alias === ''){
        	return res.status(400).send({
                'error': 'Bad Request - ALIAS must not be empty'
            })
        }


        let foundAlias = exchangeRates.find((alias) => {
            return alias.name === req.body.name && alias.ex === req.body.ex
        })

        if(foundAlias){
            return res.status(400).send({
            	'error': 'Bad Request - Duplicate Alias'
            })
        }
        if(!foundAlias){
            return res.status(200).send({
                'success': 'OK - Unique alias and complete request parameters'
            })
        }
	})
}
