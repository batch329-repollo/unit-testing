const chai = require("chai");

const expect = chai.expect;

const http = require("chai-http");
chai.use(http);

describe("api_test_suite", () => {

	it("test_api_people_is_running", () => {
		chai.request("http://localhost:5001")
		.get('/people')
		.end((error, response) => {
			expect(response).to.not.equal(undefined);
		});
	});

	it("test_api_people_returns_200", (done) => {
		chai.request("http://localhost:5001")
		.get('/people')
		.end((error, response) => {
			expect(response.status).to.equal(200);
			done();
		});
	});

	it("test_api_post_person_returns_400_if_no_person_name", (done) => {
		chai.request("http://localhost:5001")
		.post('/person')
		.type('json')
		.send({
			alias: "Jason",
			age: 28
		})
		.end((error, response) => {
			expect(response.status).to.equal(400);
			done();
		});
	});	

});

describe("api_test_suite_users", () => {

	it("test_api_users_is_running", () => {
		chai.request("http://localhost:5001")
		.get('/users')
		.end((error, response) => {
			expect(response).to.not.equal(undefined);
		});
	});

	it("test_api_post_user_returns_400_if_no_username", (done) => {
		chai.request("http://localhost:5001")
		.post('/users')
		.type('json')
		.send({
			alias: "Jason",
			age: 28
		})
		.end((error, response) => {
			expect(response.status).to.equal(400);
			done();
		});
	});

	it("test_api_post_user_returns_400_if_no_age", (done) => {
		chai.request("http://localhost:5001")
		.post('/users')
		.type('json')
		.send({
			alias: "Jason",
			name: "Jason Grill"
		})
		.end((error, response) => {
			expect(response.status).to.equal(400);
			done();
		});
	});

});
