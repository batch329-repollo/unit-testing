const { factorial, div_check } = require('../src/util.js');
const { expect, assert } = require('chai');

// Test Suites - are made up of collection of test cases that should be executed together.
describe('test_fun_factorials', () => {

	// "it()" accepts two parameters and defines a single test case.
	// 1. A string explaining what the test should do.
	// 2. Callback function w/c contains the actual set.
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	});

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		assert.equal(product, 1);
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		assert.equal(product, 24);
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		assert.equal(product, 3628800);
	});

	it('test_fun_factorial_neg1_is_undefined', () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	});

	it('test_fun_factorial_invalid_number_is_undefined', () => {
		const product = factorial("abc");
		expect(product).to.equal(undefined);
	});
});

describe('test_divisibility_by_5_or_7', () => {

	it('test_100_is_divisible_by_5', () => {
		const div = div_check(100);
		assert.equal(div, true);
	});

	it('test_49_is_divisible_by_7', () => {
		const div = div_check(49);
		assert.equal(div, true);
	});

	it('test_30_is_divisible_by_5', () => {
		const div = div_check(30);
		assert.equal(div, true);
	});

	it('test_56_is_divisible_by_7', () => {
		const div = div_check(56);
		assert.equal(div, true);
	});	
});
