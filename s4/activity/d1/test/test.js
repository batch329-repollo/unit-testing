const { factorial, div_check } = require('../src/util.js');
// const { div_check } = require('../src/util.js');

// gets the expect and assert functions from chai to be used
const {expect, assert} = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" is used to define a single test case
	// "it()" accepts two parameters
	// a string explaining what the test should do
	// callback function which contains the actual test
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})


	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1)
	})

	// In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
    it('test_fun_factorial_0!_is_1', () => {
        const product = factorial(0)
        assert.equal(product, 1)
    });

    it('test_fun_factorial_4!_is_24', () => {
        const product = factorial(4)
        expect(product).to.equal(24)
    });

    it('test_fun_factorial_10!_is_3628800', () => {
        const product = factorial(10)
        expect(product).to.equal(3628800)
    })

    // Test for negative numbers
    // RED - write a test that fails
    it("test_fun_factorials_neg1_is_undefined", () => {
        // Refactor - add the factorial function
        const product = factorial(-1);
        expect(product).to.equal(undefined);
    })

    /*
    Mini-Activity
    1. Create a failing test to test a scenario if a user enters data that is not a number.
    - test_fun_factorials_invalid_number_is_undefined
    - Assert/Expect the product to be undefined.
    2. Refactor the factorial method to accommodate the scenario of a numerical string.
    3. “abc” without factorial function
    4. Add condition in util.js
    5. Refactor the code, add factorial
    */

    // RED - write a test that fails
    it("test_fun_factorials_invalid_number_is_undefined", () => {
        // Refactor
        const product = factorial("abc");
        expect(product).to.equal(undefined);
    })

})

// Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
describe('test_divisibility_by_5_or_7', ()=>{

    it('test_100_is_divisible_by_5', () => {
        const number = div_check(100);
        expect(number).to.equal(true);
    })

    it('test_49_is_divisible_by_7', () => {
        const number = div_check(49);
        expect(number).to.equal(true);
    })  


    it('test_30_is_divisible_by_5', () => {
        const number = div_check(30);
        expect(number).to.equal(true);
    })

    it('test_56_is_divisible_by_7', () => {
        const number = div_check(56);
        expect(number).to.equal(true);
    })  

})
